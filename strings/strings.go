package strings

var (
	defaultStringIsNilPredicate = func(s string) bool {
		return len(s) == 0
	}

	defaultNilString = ""
)

func ToPointerPredicate(s string, isNil func(string) bool) *string {
	if isNil(s) {
		return nil
	}
	return &s
}

func FromPointerDefault(s *string, nilValue string) string {
	if s == nil {
		return nilValue
	}
	return *s
}

//goland:noinspection GoUnusedExportedFunction
func ToPointer(s string) *string {
	return ToPointerPredicate(s, defaultStringIsNilPredicate)
}

//goland:noinspection GoUnusedExportedFunction
func FromPointer(s *string) string {
	return FromPointerDefault(s, defaultNilString)
}
