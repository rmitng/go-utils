package methoden

import (
	"encoding/json"
)

type DeletedMessage struct {
	ID       ID         `json:"id"`
	Material MaterialID `json:"material"`
	User     UserID     `json:"user"`
}

func (c NatsClient) PublishDeleteAsync(message DeletedMessage) error {
	marshal, err := json.Marshal(message)
	if err != nil {
		return err
	}
	_, err = c.c.PublishAsync(marshal, Subject_deleted)
	if err != nil {
		return err
	}
	return nil
}

func UnmarshalDeleteMessage(b []byte) (*DeletedMessage, error) {
	var m DeletedMessage
	err := json.Unmarshal(b, &m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}
