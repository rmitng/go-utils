package methoden

import (
	"bitbucket.org/rmitng/go-utils/nats"
	natsio "github.com/nats-io/nats.go"
)

const (
	StreamName             = "METHODEN"
	StreamSubjects         = "METHODEN.*"
	Subject_status_changed = "METHODEN.status_changed"
	Subject_deleted        = "METHODEN.deleted"
)

type ID int64
type Status string
type MaterialID int64
type UserID int64

type NatsClient struct {
	c *nats.JetstreamClient
}

func NewNatsClient(url string) NatsClient {
	return NatsClient{
		c: nats.NewClient(url, StreamName, Subject_status_changed, Subject_deleted),
	}
}

func Wrap(conn *natsio.Conn) NatsClient {
	return NatsClient{
		c: nats.Wrap(conn, StreamName, Subject_status_changed, Subject_deleted),
	}
}
