package methoden

import (
	"encoding/json"
)

type StatusMessage struct {
	ID       ID         `json:"id"`
	Material MaterialID `json:"material"`
	Status   Status     `json:"status"`
	User     UserID     `json:"user"`
}

func (c NatsClient) PublishStatusAsync(message StatusMessage) error {
	marshal, err := json.Marshal(message)
	if err != nil {
		return err
	}
	_, err = c.c.PublishAsync(marshal, Subject_status_changed)
	if err != nil {
		return err
	}
	return nil
}

func UnmarshalStatusMessage(b []byte) (*StatusMessage, error) {
	var m StatusMessage
	err := json.Unmarshal(b, &m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}
