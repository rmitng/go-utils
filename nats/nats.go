package nats

import (
	"github.com/nats-io/nats.go"
)

type JetstreamClient struct {
	url        string
	nc         *nats.Conn
	js         nats.JetStreamContext
	streamName string
	subjects   []string
}

func (r *JetstreamClient) conn() (*nats.Conn, error) {
	if r.nc == nil {
		nc, err := nats.Connect(r.url)
		if err != nil {
			return nil, err
		}
		r.nc = nc
	}
	return r.nc, nil
}

func (r *JetstreamClient) context() (nats.JetStreamContext, error) {

	if r.js == nil {
		// Connect to NATS
		nc, err := r.conn()
		if err != nil {
			return nil, err
		}

		// Create JetStream Context
		js, err := nc.JetStream(nats.PublishAsyncMaxPending(256))
		if err != nil {
			return nil, err
		}
		r.js = js

		if err = r.stream(); err != nil {
			return nil, err
		}

	}
	return r.js, nil

}

func (r *JetstreamClient) stream() error {
	_, err := r.js.StreamInfo(r.streamName)
	if err != nil {
		if err == nats.ErrStreamNotFound {
			// stream not found, create it
			_, err = r.js.AddStream(&nats.StreamConfig{
				Name:     r.streamName,
				Subjects: r.subjects,
			})

			if err != nil {
				return err
			}
		}
		return err
	}
	return nil
}

func (r *JetstreamClient) PublishAsync(msg []byte, subject string) (nats.PubAckFuture, error) {
	// Create JetStream Context
	js, err := r.context()

	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}
	ack, err := js.PublishAsync(subject, msg)
	if err != nil {
		return nil, err
	}

	return ack, nil
}

func (r *JetstreamClient) Close() error {
	if r.nc != nil {
		r.nc.Close()
		r.js = nil
	}
	return nil
}

// NewClient
func NewClient(url string, stream string, subjects ...string) *JetstreamClient {
	return &JetstreamClient{
		url:        url,
		nc:         nil,
		js:         nil,
		streamName: stream,
		subjects:   subjects,
	}
}

func Wrap(conn *nats.Conn, stream string, subjects ...string) *JetstreamClient {
	return &JetstreamClient{
		url:        conn.ConnectedUrl(),
		nc:         conn,
		js:         nil,
		streamName: stream,
		subjects:   subjects,
	}
}
