package material

import (
	"encoding/json"
)

type DeleteMessage struct {
	ID      ID                `json:"id"`
	Auftrag AuftragSchluessel `json:"auftrag"`
	UserID  UserID            `json:"userID"`
}

func (c NatsClient) PublishDeleteAsync(message DeleteMessage) error {
	marshal, err := json.Marshal(message)
	if err != nil {
		return err
	}
	_, err = c.c.PublishAsync(marshal, Subject_deleted)
	if err != nil {
		return err
	}
	return nil
}

func UnmarshalDeleteMessage(b []byte) (*DeleteMessage, error) {
	var m DeleteMessage
	err := json.Unmarshal(b, &m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}
