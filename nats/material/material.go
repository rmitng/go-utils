package material

import (
	"bitbucket.org/rmitng/go-utils/nats"
	natsio "github.com/nats-io/nats.go"
)

const (
	StreamName             = "MATERIAL"
	StreamSubjects         = "MATERIAL.*"
	Subject_status_changed = "MATERIAL.status_changed"
	Subject_deleted        = "MATERIAL.deleted"
)

type ID int64
type Status string
type UserID int64
type AuftragSchluessel struct {
	Nummer int `json:"nummer"`
	Jahr   int `json:"jahr"`
}

type NatsClient struct {
	c *nats.JetstreamClient
}

func NewNatsClient(url string) NatsClient {
	return NatsClient{
		c: nats.NewClient(url, StreamName, Subject_status_changed, Subject_deleted),
	}
}

func Wrap(conn *natsio.Conn) NatsClient {
	return NatsClient{
		c: nats.Wrap(conn, StreamName, Subject_status_changed, Subject_deleted),
	}
}
