package print

import (
	"bitbucket.org/rmitng/go-utils/nats"
	"encoding/json"
	natsio "github.com/nats-io/nats.go"
)

const (
	StreamName     = "FILEDATA"
	StreamSubjects = "FILEDATA.*"
	Subject_print  = "FILEDATA.print"
)

type Druckkontext string
type Druckertyp string
type PrintMessage struct {
	Filename     string       `json:"filename"`
	Data         []byte       `json:"data"`
	Druckkontext Druckkontext `json:"druckkontext"`
	Druckertyp   Druckertyp   `json:"druckertyp"`
}

func (c NatsClient) PublishPrintAsync(message PrintMessage) error {
	marshal, err := json.Marshal(message)
	if err != nil {
		return err
	}
	_, err = c.c.PublishAsync(marshal, Subject_print)
	if err != nil {
		return err
	}
	return nil
}

type NatsClient struct {
	c *nats.JetstreamClient
}

func NewNatsClient(url string) NatsClient {
	return NatsClient{
		c: nats.NewClient(url, StreamName, Subject_print),
	}
}

func Wrap(conn *natsio.Conn) NatsClient {
	return NatsClient{
		c: nats.Wrap(conn, StreamName, Subject_print),
	}
}

func Unmarshal(b []byte) (*PrintMessage, error) {
	var m PrintMessage
	err := json.Unmarshal(b, &m)
	if err != nil {
		return nil, err
	}
	return &m, nil
}
