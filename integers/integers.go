package integers

var (
	negativeInt64IsNil = func(i int64) bool {
		return i < 0
	}
	negativeInt32IsNil = func(i int32) bool {
		return i < 0
	}
	negativeIntIsNil = func(i int) bool {
		return i < 0
	}

	defaultInt64NilValue = int64(-1)
	defaultInt32NilValue = int32(-1)
	defaultIntNilValue   = -1
)

//goland:noinspection GoUnusedExportedFunction
func ToInt64Pointer(i int64) *int64 {
	return ToInt64PointerPredicated(i, negativeInt64IsNil)
}

//goland:noinspection GoUnusedExportedFunction
func ToInt32Pointer(i int32) *int32 {
	return ToInt32PointerPredicated(i, negativeInt32IsNil)
}

//goland:noinspection GoUnusedExportedFunction
func ToIntPointer(i int) *int {
	return ToIntPointerPredicated(i, negativeIntIsNil)
}

func ToInt64PointerPredicated(i int64, isNil func(int64) bool) *int64 {
	if isNil(i) {
		return nil
	}
	return &i
}

func ToInt32PointerPredicated(i int32, isNil func(int32) bool) *int32 {
	if isNil(i) {
		return nil
	}
	return &i
}

func ToIntPointerPredicated(i int, isNil func(int) bool) *int {
	if isNil(i) {
		return nil
	}
	return &i
}

func FromInt64PointerDefault(i *int64, nilValue int64) int64 {
	if i == nil {
		return nilValue
	}
	return *i
}

func FromInt32PointerDefault(i *int32, nilValue int32) int32 {
	if i == nil {
		return nilValue
	}
	return *i
}

func FromIntPointerDefault(i *int, nilValue int) int {
	if i == nil {
		return nilValue
	}
	return *i
}

//goland:noinspection GoUnusedExportedFunction
func FromInt64Pointer(i *int64) int64 {
	return FromInt64PointerDefault(i, defaultInt64NilValue)
}

//goland:noinspection GoUnusedExportedFunction
func FromInt32Pointer(i *int32) int32 {
	return FromInt32PointerDefault(i, defaultInt32NilValue)
}

//goland:noinspection GoUnusedExportedFunction
func FromIntPointer(i *int) int {
	return FromIntPointerDefault(i, defaultIntNilValue)
}
