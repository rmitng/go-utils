package integers

import (
	"reflect"
	"testing"
)

func TestToInt64Pointer(t *testing.T) {
	type args struct {
		i int64
	}
	p := int64(1)
	tests := []struct {
		name string
		args args
		want *int64
	}{
		{
			name: "positive to pointer",
			args: args{i: p},
			want: &p,
		},
		{
			name: "negative to nil",
			args: args{i: -1},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ToInt64Pointer(tt.args.i); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ToInt64Pointer() = %v, want %v", got, tt.want)
			}
		})
	}
}
