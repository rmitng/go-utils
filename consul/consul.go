package consul

import (
	"github.com/hashicorp/consul/api"
	"os"
)

const (
	// DefaultUrl the default server URL
	DefaultUrl = "localhost:8500"
	// DefaultScheme the default server scheme
	DefaultScheme = "http"
)

// Client provides an interface for getting data out of Consul
type Client interface {
	// Service Get a Service from api
	Service(string) (*api.AgentService, *api.QueryMeta, error)
	// Register a service with local agent
	Register(string, int) error
	// DeRegister a service with local agent
	DeRegister(string) error
}

type client struct {
	consul *api.Client
}

// NewConsulClient returns a Client interface for given api address
func NewConsulClient(scheme string, addr string) (Client, error) {
	config := api.DefaultConfig()
	config.Address = addr
	config.Scheme = scheme
	c, err := api.NewClient(config)
	if err != nil {
		return nil, err
	}
	return &client{consul: c}, nil
}

// Register a service with api local agent
func (c *client) Register(name string, port int) error {
	return c.RegisterWithHostname(name, GetHostname(), port)
}

func (c *client) RegisterWithHostname(name string, hostname string, port int) error {
	reg := &api.AgentServiceRegistration{
		ID:      name,
		Name:    name,
		Port:    port,
		Address: hostname,
	}
	return c.consul.Agent().ServiceRegister(reg)
}

// DeRegister a service with api local agent
func (c *client) DeRegister(id string) error {
	return c.consul.Agent().ServiceDeregister(id)
}

// Service return a service
func (c *client) Service(service string) (*api.AgentService, *api.QueryMeta, error) {
	agentService, meta, err := c.consul.Agent().Service(service, nil)
	if err != nil {
		return nil, nil, err
	}
	return agentService, meta, nil
}

func GetHostname() (hostname string) {
	hostname, _ = os.Hostname()
	return
}
