package errors

import (
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

//goland:noinspection GoUnusedGlobalVariable
var (
	ErrInvalidArgument = &apperror{
		message: "invalid function argument(s)",
		code:    codes.InvalidArgument,
	}
	ErrNotFound = &apperror{
		message: "entity not found",
		code:    codes.NotFound,
	}
	ErrFailedPrecondition = &apperror{
		message: "a precondition failed",
		code:    codes.FailedPrecondition,
	}
)

type (
	Coder interface {
		Code() codes.Code
	}

	apperror struct {
		message string
		code    codes.Code
		cause   error
	}
)

func (a *apperror) Code() codes.Code {
	return a.code
}

func (a *apperror) Error() string {
	return a.message
}

func (a *apperror) Unwrap() error {
	return a.cause
}

//goland:noinspection GoUnusedExportedFunction
func NewInternal(msg string, cause error) error {
	return &apperror{
		message: msg,
		cause:   cause,
		code:    codes.Internal,
	}
}

//goland:noinspection GoUnusedExportedFunction
func NewFailedPrecondition(msg string, cause error) error {
	return &apperror{
		message: msg,
		cause:   cause,
		code:    codes.FailedPrecondition,
	}
}

//goland:noinspection GoUnusedExportedFunction
func NewInvalidArgument(msg string, cause error) error {
	return &apperror{
		message: msg,
		cause:   cause,
		code:    codes.InvalidArgument,
	}
}

func New(msg string, code codes.Code, cause error) error {
	return &apperror{
		message: msg,
		cause:   cause,
		code:    code,
	}
}

//goland:noinspection GoUnusedExportedFunction
func EncodeGrpcError(err error) error {
	if err == context.DeadlineExceeded {
		return status.Error(
			codes.DeadlineExceeded,
			err.Error(),
		)
	}
	if coder, ok := err.(Coder); ok {
		return status.Error(
			coder.Code(),
			err.Error(),
		)
	}
	return NewInternal(err.Error(), err)
}
