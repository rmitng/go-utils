package time

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"github.com/rickb777/date"
	"time"
)

type Datum struct {
	date date.Date
}

//goland:noinspection GoUnusedExportedFunction
func NeuesDatum(year int, month time.Month, day int) *Datum {
	return &Datum{
		date: date.New(year, month, day),
	}
}

//goland:noinspection GoUnusedExportedFunction
func Heute() *Datum {
	return &Datum{date.Today()}
}

func (t Datum) Valid() bool {
	return t.date.Year() > 0 && t.date.Month() > 0 && t.date.Month() < 13 && t.date.Day() > 0 && t.date.Day() < 32
}

func (t Datum) MarshalJSON() ([]byte, error) {
	if t.Valid() {
		return json.Marshal(t.String())
	}
	return json.Marshal(nil)
}

func (t *Datum) UnmarshalJSON(s []byte) (err error) {
	// Unmarshalling into a pointer will let us detect null
	var u string
	if err := json.Unmarshal(s, &u); err != nil {
		return err
	}
	parsed, err := date.Parse(patternDate, u)
	if err != nil {
		return err
	}
	t.date = parsed
	return nil
}

func (t Datum) String() string {
	return t.execIfValid(func(t2 Datum) string {
		return t2.date.Format(patternDate)
	})
}

func (t Datum) Value() (driver.Value, error) {
	if !t.Valid() {
		return nil, errors.New("date is not valid")
	}
	return t.date.String(), nil
}

func (t *Datum) Scan(value interface{}) (err error) {
	return t.date.Scan(value)
}

func (t Datum) execIfValid(f1 func(Datum) string) string {
	if t.Valid() {
		return f1(t)
	}
	return ""
}

func (t *Datum) Jahr() int {
	return t.date.Year()
}

func (t *Datum) Monat() time.Month {
	return t.date.Month()
}

func (t *Datum) Tag() int {
	return t.date.Day()
}
