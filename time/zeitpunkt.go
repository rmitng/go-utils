package time

import (
	"bitbucket.org/rmitng/go-utils/errors"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"google.golang.org/grpc/codes"
	"time"
)

type Zeitpunkt struct {
	t time.Time
}

//goland:noinspection GoUnusedExportedFunction
func NeuerZeitpunkt(jahr int, monat time.Month, tag int, std int, min int) *Zeitpunkt {
	return &Zeitpunkt{time.Date(jahr, monat, tag, std, min, 0, 0, Loc)}
}

//goland:noinspection GoUnusedExportedFunction
func Jetzt() *Zeitpunkt {
	return &Zeitpunkt{time.Now()}
}

func (t Zeitpunkt) Unix() int64 {
	return t.t.Unix()
}

func (t Zeitpunkt) Valid() bool {
	return t.t.Year() > 0 && t.t.Month() > 0 && t.t.Month() < 13 && t.t.Day() > 0 && t.t.Day() < 32
}

func (t Zeitpunkt) MarshalJSON() ([]byte, error) {
	if t.Valid() {
		return json.Marshal(t.Unix())
	}
	return json.Marshal(nil)
}

func (t *Zeitpunkt) UnmarshalJSON(s []byte) (err error) {
	// Unmarshalling into a pointer will let us detect null
	var u *int64
	if err := json.Unmarshal(s, &u); err != nil {
		return err
	}
	if u == nil {
		return errors.New("Could not unmarshal nil Zeitpunkt", codes.InvalidArgument, nil)
	}
	t.t = time.Unix(*u, 0)
	return nil
}

func (t Zeitpunkt) String() string {
	return t.execIfValid(func(t2 Zeitpunkt) string {
		return t.t.Format(patternZeitpunkt)
	})
}

func (t Zeitpunkt) Value() (driver.Value, error) {
	if !t.Valid() {
		return nil, errors.New("date is not valid", codes.Internal, nil)
	}
	return t.String(), nil
}

func (t *Zeitpunkt) Scan(value interface{}) (err error) {
	if value == nil {
		return
	}
	switch v := value.(type) {
	case time.Time:
		t.t = v
		return
	}

	return fmt.Errorf("can't convert %T to time.Zeitpunkt", value)
}

func (t Zeitpunkt) execIfValid(f1 func(Zeitpunkt) string) string {
	if t.Valid() {
		return f1(t)
	}
	return ""
}

func (t Zeitpunkt) Jahr() int {
	return t.t.Year()
}

func (t Zeitpunkt) Monat() time.Month {
	return t.t.Month()
}

func (t Zeitpunkt) Tag() int {
	return t.t.Day()
}

func (t Zeitpunkt) Stunde() int {
	return t.t.Hour()
}

func (t Zeitpunkt) Minute() int {
	return t.t.Minute()
}
