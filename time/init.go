package time

import (
	"fmt"
	"time"
)

const (
	patternHHMM = "15:04"
	//	patternHHMMSS    = "15:04:05"
	patternDate      = "2006-01-02"
	patternZeitpunkt = patternDate + " " + patternHHMM
)

// -----------------------------------------------------------------------

// Loc Location for all dates and times
//goland:noinspection ALL
var Loc *time.Location

func init() {
	var err error
	Loc, err = time.LoadLocation("Europe/Berlin")
	if err != nil {
		fmt.Printf("Could not load timezone Europe/Berlin, using UTC instead: %v\n", err)
		Loc = time.UTC
	}
}
