package time

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"github.com/rickb777/date/clock"
	"strings"
	"time"
)

type Uhrzeit struct {
	c clock.Clock
}

//goland:noinspection GoUnusedExportedFunction
func AktuelleUhrzeit() *Uhrzeit {
	now := clock.NewAt(time.Now()).Mod24()
	return &Uhrzeit{now}
}

func (t Uhrzeit) Valid() bool {
	return t.c != clock.Undefined && t.c.Hours() >= 0 && t.c.Hours() < 24 && t.c.Minutes() >= 0 && t.c.Minutes() < 60
}

func (t Uhrzeit) MarshalJSON() ([]byte, error) {
	if t.Valid() {
		return json.Marshal(t.String())
	}
	return json.Marshal(nil)
}

func (t *Uhrzeit) UnmarshalJSON(s []byte) (err error) {
	// Unmarshalling into a pointer will let us detect null
	var v string
	if err := json.Unmarshal(s, &v); err != nil {
		return err
	}
	if len(strings.TrimSpace(v)) == 0 {
		t.c = clock.Undefined
	} else {
		parsed, err := clock.Parse(v)
		if err != nil {
			return err
		}
		t.c = parsed
	}
	return
}

func (t Uhrzeit) String() string {
	return t.c.HhMm()
}

func (t Uhrzeit) Value() (driver.Value, error) {
	if !t.Valid() {
		return nil, nil
	}
	return t.String(), nil
}

func (t *Uhrzeit) Scan(value interface{}) (err error) {
	return t.c.Scan(value)
}

//goland:noinspection GoUnusedExportedFunction
func NeueUhrzeit(h int, m int) (*Uhrzeit, error) {
	t := Uhrzeit{
		clock.New(h, m, 0, 0),
	}
	if t.Valid() {
		return &t, nil
	}
	return nil, fmt.Errorf("uhrzeit %s is out of range [0,23]:[0,59]", t.String())
}

func (t Uhrzeit) Stunde() int {
	return t.c.Hours()
}

func (t Uhrzeit) Minute() int {
	return t.c.Minutes()
}
